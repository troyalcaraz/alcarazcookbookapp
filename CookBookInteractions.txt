Troy Alcaraz

Cook Book App Interactions - Adding a favorite

Adding a Favorite - via Creating/Adding meal to "My Cook Book"

User opens the app
	- Initiate the app
User chooses "My Cook Book"
	- Choose out of 3
		* "My Pantry"
		* "My Cook Book"
		* "Community"
App pulls up the user's cook book
User chooses "Add new meal"
	- Initiate making a new meal
User adds all the instructions and ingredients to the new meal
	- Input a string of the name of the meal and a description
	- Input List of instructions on how to prepare dish
		* Ingredients
		* Utensils
		* Recipe
	- Optional: Add a video tutorial how to make the dish
	- Optional: Add a photo of the meal
User finishes creating the meal
App asks user if they would like to save/cancel new meal to favorites
	- Choose out of 2
		* Done
		* Cancel
App marks the meal as a favorite
App presents list of favorites

Adding a Favorite - via Editing an existing meal

User opens the app
	- Initiate the app
User chooses "My Cook Book"
	- Choose out of 3
		* "My Pantry"
		* "My Cook Book"
		* "Community"
App pulls up the user's cook book
User chooses a meal they want to save to favorites
	- Choose from a list of a couple to a lot
App pulls up details on chosen meal
App asks user if they would like to save/cancel new meal to favorites
	- Choose out of 2
		* Add to favorites
		* Cancel
User chooses save!
App marks the meal as a favorite

